# 安装过程
## 进入dockefile目录，执行以下命令
## php8下面gd安装，容易出现libzip不存在或者版本不对得问题，目录下的libzip为1.2.0版本，可以使用
docker build -t ityun/centos7-php8 .   

## 拉取redis、mysql5.7镜像
docker pull redis
docker pull mysql:5.7


# 启动Docker容器
## 启动redis容器
docker run -it -d -p 8379:6379 --name redis  redis:latest
## 启动mysql容器 
docker run -it -d -p 8306:3306 -e MYSQL_ROOT_PASSWORD=root --name mysqldb  -v ~/Documents/mysql57:/var/lib/mysql mysql:5.7

## 通过上面步骤下面这个对应的link名称自己改一下
docker run -it -p 8472:80 -p 8473:443 --name ityun --link mysqldb:mysqldb --link redis:redis --link -d -v ~/Documents/code:/web ityun/centos7-php8


## 进入容器
docker exec -it ityun bash

### ps:以下是windows系统的启动 可忽略
docker run -it -d -p 8379:6379 --name redis  redis:latest
docker run -it -d -p 8306:3306 -e MYSQL_ROOT_PASSWORD=root --name mysqldb  -v E://project/docker/docker-data/mysql57:/var/lib/mysql mysql:5.7

docker run -it -p 8472:80 -p 8473:443 --name ityun --link mysqldb:mysqldb --link redis:redis -d -v E://project/docker/code:/web ityun/centos7-php8


# 配置docker容器内访问宿主机mysql redis
## mysql 可指定特定ip值
update user set host = '%' where user = 'root';
FLUSH PRIVILEGES;

## redis
将redis.conf文件中bind 127.0.0.1注释掉，或者添加指定ip


# 配置Nginx支持pathinfo模式
    ``` 
    location ~ \.php(.*)$ {
        fastcgi_pass   127.0.0.1:9000;
        fastcgi_index  index.php;
        fastcgi_split_path_info  ^((?U).+\.php)(/?.+)$;
        fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
        fastcgi_param  PATH_INFO  $fastcgi_path_info;
        fastcgi_param  PATH_TRANSLATED  $document_root$fastcgi_path_info;
        include        fastcgi_params;
    }
    ```