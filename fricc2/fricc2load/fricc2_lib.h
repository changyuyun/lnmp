/*
   +----------------------------------------------------------------------+
   | FRICC2                                                               |
   +----------------------------------------------------------------------+
   | Author: sun bing <hoowa.sun@gmail.com>                               |
   +----------------------------------------------------------------------+
*/
#ifndef FRICC2_LIB_H
#define FRICC2_LIB_H

#define FRICCTAG_STR	"qimingcxkm"

#define FRICCTAG_LEN	10

// Notice FRICCKEY must more than 16 numbers
#define FRICCKEY 10336,29600,14479,25101,22563,8617,23703,20111,25197,14006,19760,23675,3315,19692,21418,29374

#define ZENCOMPRESS	0
#define ZDECOMPRESS	1
#define OUTBUFSIZ  100000

void fricc2_lib_decrypt(char *file_buf, size_t *file_buf_len);
char *fricc2_lib_zcodecom(int mode, char *inbuf, size_t inbuf_len, size_t *resultbuf_len);

#endif