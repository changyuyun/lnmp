FROM centos:7
MAINTAINER ityun

ENV TIME_ZONE Asia/Shanghai
#基础依赖
RUN yum install -y install epel-release && \
    yum -y install gcc gcc-c++  autoconf wget libxml2 libxml2-devel openssl openssl-devel curl curl-devel libjpeg-turbo libjpeg-turbo-devel libpng-devel libpng freetype-devel freetype icu libicu-devel libicu libmcrypt libmcrypt-devel libxslt libxslt-devel php-mysql oniguruma oniguruma-devel libtidy-devel openldap openldap-devel sqlite-devel zlib-static zlib-devel && \
    yum clean all && \
    groupadd www && \
    useradd -g www www  

RUN cat .pwd.txt | chpasswd

#拷贝源码包到环境中
ADD php-8.0.10.tar.gz /usr/local/src/
ADD redis-5.3.4.tgz /usr/local/src/
ADD nginx-1.21.1.tar.gz /usr/local/src/
ADD libzip-1.2.0.tar.gz /usr/local/src/
COPY fricc2 /usr/local/src/php-fricc2/
COPY nginx.conf /usr/local/src/nginx.conf
COPY cas.ityun.com_80.conf /usr/local/src/cas.ityun.com_80.conf
COPY php.ini /usr/local/src/php.ini
COPY fileinfo /usr/local/src/fileinfo/

#编译安装nginx及php
RUN cd /usr/local/src/nginx-1.21.1 && \
    ./configure --prefix=/usr/local/nginx --user=www --group=www --with-stream --with-http_ssl_module --with-http_stub_status_module && \
    make -j 4 && \
    make install && \
    mkdir -p /web && \
    cd /usr/local/nginx/conf/ && \
    mkdir vhosts && \
    cp /usr/local/src/cas.ityun.com_80.conf /usr/local/nginx/conf/vhosts/ &&\
    cp /usr/local/src/nginx.conf /usr/local/nginx/conf/ &&\
    cp -frp /usr/lib64/libldap* /usr/lib/ &&\
    #安装libzip依赖，方便gd开启
    cd /usr/local/src/libzip-1.2.0 && \
    ./configure && make && make install && \
    export PKG_CONFIG_PATH="/usr/local/lib/pkgconfig/" &&\
    #安装php
    cd /usr/local/src/php-8.0.10 && \
    ./configure --prefix=/usr/local/php8 --with-config-file-path=/usr/local/php8/etc \
    --with-config-file-scan-dir=/usr/local/php8/conf.d \
    --enable-fpm --with-fpm-user=www \
    --with-fpm-group=www --enable-static --enable-sockets \
    --enable-mysqlnd --enable-opcache --enable-pcntl --enable-gd \
    --enable-mbstring --enable-soap  --enable-calendar \
    --enable-bcmath --enable-exif --enable-ftp --enable-intl --with-mysqli --with-pdo-mysql  --without-sqlite3 \
    --with-openssl --with-curl --with-gettext  --with-zip --with-zlib \
    --with-mhash  --with-tidy  \
    --disable-fileinfo \
    --with-freetype \
    --with-jpeg && \
    make && \
    make install && \
    ln -s /usr/local/php8/bin/php /usr/bin/php && \
    #配置相关
    cp /usr/local/php8/etc/php-fpm.d/www.conf.default /usr/local/php8/etc/php-fpm.d/www.conf && \
    cp /usr/local/php8/etc/php-fpm.conf.default /usr/local/php8/etc/php-fpm.conf && \
    sed -i "90a \daemonize = no" /usr/local/php8/etc/php-fpm.conf && \
    cp /usr/local/src/php-8.0.10/sapi/fpm/init.d.php-fpm /etc/init.d/php-fpm && \
    chmod +x /etc/init.d/php-fpm && \
    echo "${TIME_ZONE}" > /etc/timezone && \
	ln -sf /usr/share/zoneinfo/${TIME_ZONE} /etc/localtime && \
    #安装redis扩展
    cd /usr/local/src/redis-5.3.4 && \
    /usr/local/php8/bin/phpize &&\
    ./configure --with-php-config=/usr/local/php8/bin/php-config &&\
    make -j 4 && make install &&\
    #安装ldap扩展
    cd /usr/local/src/php-8.0.10/ext/ldap/ &&\
    /usr/local/php8/bin/phpize && ./configure --with-php-config=/usr/local/php8/bin/php-config  --with-ldap &&\
    make && make install &&\
    #安装fileinfo扩展#
    cd /usr/local/src/php-8.0.10/ext/fileinfo/ &&\
    /usr/local/php8/bin/phpize && ./configure --with-php-config=/usr/local/php8/bin/php-config &&\
    cp -rf /usr/local/src/fileinfo/Makefile /usr/local/src/php-8.0.10/ext/fileinfo/ &&\
    make && make install &&\
    #安装fricc2扩展
    cd /usr/local/src/php-fricc2/fricc2load/ &&\
    /usr/local/php8/bin/phpize && ./configure --with-php-config=/usr/local/php8/bin/php-config &&\
    make && make install &&\
    cd /usr/local/src/php-fricc2/fricc2/ && make &&\
    cp fricc2 /usr/bin &&\
    cp /usr/local/src/php.ini /usr/local/php8/etc/php.ini &&\
    rm -rf /usr/local/src/*


WORKDIR /usr/local/nginx
EXPOSE 9000
EXPOSE 80
EXPOSE 443

CMD /etc/init.d/php-fpm start && /usr/local/nginx/sbin/nginx -g "daemon off;"